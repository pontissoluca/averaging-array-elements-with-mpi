EXECS=test_mpi_array
MPICC?=mpicc

CCFLAGS= -I ./include
LDFLAGS= -lm

all: ${EXECS}

test_mpi_array: test_mpi_array.c
	${MPICC} -o test_mpi_array test_mpi_array.c $(LDFLAGS) $(CCFLAGS)

clean:
	rm ${EXECS}
