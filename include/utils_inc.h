#ifndef UTILS_INC_H__
#define UTILS_INC_H__


float *createArray2D (int dimrow, int dimcol) {
    float *array2D;
    int h, i, j;

    if (( array2D = malloc(dimrow*dimcol*sizeof(float))) == NULL) {
        printf("... error in creating 2D array\n");
        exit(EXIT_FAILURE);
    }

    return array2D;
}

void printArray (float *arr_in, int dimrow, int dimcol, int row_start, int row_end) { //
  int indrow, indcol;
    for (indrow=0+row_start; indrow<dimrow-row_end; indrow++) {
      for (indcol=0; indcol<dimcol; indcol++) {
        printf("%f\t", arr_in[indrow*dimcol+indcol]);
	if (indcol==dimcol-1) printf("\n");
      }
    }
}//


void dumpArray (float *arr_in, int dimrow, int dimcol, char *file_output) { //
  int indrow, indcol;
  FILE *fptr;
  
  fptr=fopen(file_output,"w");
  if (!fptr)
    printf("Cannot open file %s\n", file_output);

  for (indrow=0; indrow<dimrow; indrow++) {
    for (indcol=0; indcol<dimcol; indcol++) {
      fprintf(fptr, "%f\t", arr_in[indrow*dimcol+indcol]);
      if (indcol==dimcol-1) fprintf(fptr, "\n");
    }
  }
  
  fclose(fptr);
}//


float *comp_filter(float *arr_in, float *arr_out, int dimrow, int dimcol, int step){
  
  int indrow, indcol, ind_st_r, ind_st_c, res_c=0;
  float accum=0.f;
  int kernel_dim=step*2+1; //kernel_dim=side of a square centered on a single array element
  float norm = pow((double)kernel_dim,2.f);
  
  for (indrow=0; indrow<dimrow;indrow++)
    { //loopC
      for (indcol=0; indcol<dimcol;indcol++){ //loopD
	if ( ((indrow-step) >=0 && (indrow+step <= dimrow-1)) &&  ((indcol-step) >=0 && (indcol+step <= dimcol-1)) ) { //ifA
	  
	  for (ind_st_r=indrow-step;ind_st_r<=indrow+step;ind_st_r++)
	    for (ind_st_c=indcol-step;ind_st_c<=indcol+step;ind_st_c++)
	      accum+=arr_in[ind_st_r*dimcol+ind_st_c];
	  
	} //ifA
	else if( (indrow-step) < 0){ //elseifA
	  for (ind_st_r=indrow+step;ind_st_r>=indrow-step+abs(indrow-step);ind_st_r--){ //loopE
	    if (indcol-step < 0){ //ifB
	      for (ind_st_c=indcol+step;ind_st_c>=indcol-step+abs(indcol-step);ind_st_c--){
		//printf("---> indcol-step: %d [%d][%d] [%d]\n", (indcol-step), indrow, indcol, ind_st_r);
		accum+=arr_in[ind_st_r*dimcol+ind_st_c];
	      }
	    } //ifB
	    else if ( (indcol-step) >=0 ) { //elseifB
	      res_c=(indcol+step <= dimcol-1)?0:(indcol+step-(dimcol-1));
	      for (ind_st_c=indcol-step;ind_st_c<=indcol+step-res_c;ind_st_c++){
		//printf("---> indcol-step: %d [%d][%d] [%d]\n", (indcol-step), indrow, indcol, ind_st_r);
		accum+=arr_in[ind_st_r*dimcol+ind_st_c];
		  }
	      
	    } //elseifB
		  
	  } //loopE
	  
	} //elseifA
	else if (indcol-step < 0 && (indrow+step <= dimrow-1)){ //elseifAA
	  for (ind_st_r=indrow-step;ind_st_r<=indrow+step;ind_st_r++){
	    for (ind_st_c=indcol+step;ind_st_c>=indcol-step+abs(indcol-step);ind_st_c--){
	      //printf("---> indcol-step: %d [%d][%d] [%d]\n", (indcol-step), indrow, indcol, ind_st_r);
	      accum+=arr_in[ind_st_r*dimcol+ind_st_c];
	    }
	  }
	} //elseifAA
	else if ( ((indcol+step) > (dimcol-1)) && (indrow+step <= dimrow-1)) { //elseifAAA
	  res_c=(indcol+step <= dimcol-1)?0:(indcol+step-(dimcol-1));
	  for (ind_st_r=indrow-step;ind_st_r<=indrow+step;ind_st_r++){
	    for (ind_st_c=indcol-step;ind_st_c<=indcol+step-res_c;ind_st_c++){
	      //printf("---> indcol-step: %d [%d][%d] [%d]\n", (indcol-step), indrow, indcol, ind_st_r);
	      accum+=arr_in[ind_st_r*dimcol+ind_st_c];
	    }
	  }
	} //elseifAAA
	
	
	else if( (indrow+step)>(dimrow-1) ){ //elseifAAAA
	      for (ind_st_r=indrow-step;ind_st_r<=dimrow-1;ind_st_r++){ //
	      	if (indcol-step < 0){ //ifB
	      	  for (ind_st_c=indcol+step;ind_st_c>=indcol-step+abs(indcol-step);ind_st_c--){
		    //printf("---> indcol-step: %d [%d][%d] [%d]\n", (indcol-step), indrow, indcol, ind_st_r);
		    accum+=arr_in[ind_st_r*dimcol+ind_st_c];
		  }
		} //ifB
		else if ( (indcol-step) >=0 ) {
		  res_c=(indcol+step <= dimcol-1)?0:(indcol+step-(dimcol-1));
		  for (ind_st_c=indcol-step;ind_st_c<=indcol+step-res_c;ind_st_c++){
		    //printf("---> indcol-step: %d [%d][%d] [%d]\n", (indcol-step), indrow, indcol, ind_st_r);
		    accum+=arr_in[ind_st_r*dimcol+ind_st_c];
		  }
		}
	      } //
	} //elseifAAAA
	
	arr_out[indrow*dimcol+indcol]=accum/norm;
	accum=0;

	/* fprintf(fptr,"%f\t", arr_out[indrow*dimcol+indcol]); */
	/* if ( indcol==(dimcol-1) ) {fprintf(fptr, "\n");} */

      } //loopD
    } //loopC
  
}


float **floatalloc2d(int n, int m) {
    float *data = (float *)malloc(n*m*sizeof(float));
    if (data==NULL){
      printf("...failure in allocating memory\n");
      exit(EXIT_FAILURE);
    }
    
    float **array = (float **)calloc(n, sizeof(float *));
    for (int i=0; i<n; i++)
        array[i] = &(data[i*m]);

    return array;
}

float floatfree2d(float **array) {
    free(array[0]);
    free(array);
    return 0;
}






#endif /*  UTILS_INC_H__ */
