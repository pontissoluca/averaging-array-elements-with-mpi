#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <mpi.h>
#include "utils_inc.h"


#define DIMROW 10
#define DIMCOL 7
#define STEP 2
#define TYPE float

int main(int argc, char **argv)
{
  int ierr, num_procs, my_id, ind, ind_br, indrow, indcol, cntelem=0;
  int ind_st_r, ind_st_c, res_c=0;
  int root_process=0;
  int kernel_dim=STEP*2+1; //kernel_dim=side of a square centered on a single array element
  TYPE norm = pow((double)kernel_dim,2.f);
  TYPE accum=0, sum=0;
  TYPE *arr_in=NULL, *arr_out=NULL, *arr_global=NULL, *arr_comm=NULL, *arr_res=NULL;
  TYPE valreset=0;
  char *file_array_out=NULL;
  //char file_output[FILENAME_MAX];
  char file_arr_out[]="file_arr_out.txt";
  char file_arr_global[]="file_arr_global.txt";
    
  int dimrow=DIMROW, dimcol=DIMCOL;
  int modefill=0;
  

  while(1) {
    char c = getopt (argc, argv, "hm:");
    if(c == -1)
      break;
    
    switch(c) {
      
    case 'm':
      modefill=atoi(optarg);
      break;
    case 'h':
      printf("Usage: %s -m <array input filled with 0: random [0-1], 1: ones, 2: ramp\n", argv[0]);
      exit(0);
    default:
      printf("Usage: %s -h for help\n", argv[0]);
      exit(0);
    }
  }
  
  
  ierr = MPI_Init(&argc, &argv);
  if (ierr!=MPI_SUCCESS){
    printf("MPI routine MPI_Init() failed\n");
      exit(EXIT_FAILURE);
      };
  
  /* find out MY process ID, and how many processes were started. */
  
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
  if (ierr!=MPI_SUCCESS){
    printf("MPI routine MPI_Comm_rank() failed\n");
      exit(EXIT_FAILURE);
  };

  ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  if (ierr!=MPI_SUCCESS){
    printf("MPI routine MPI_Comm_size() failed\n");
      exit(EXIT_FAILURE);
  };

 
  /* printf("[MPI process %d] I start waiting on the barrier.\n", my_id); */
  /* MPI_Barrier(MPI_COMM_WORLD); */

  /* printf("I'm process %i out of %i processes\n", my_id, num_procs); */
  
  int dim_arr_comm=0, len_r_arr_comm=0, rem_r=0;
  len_r_arr_comm=dimrow/num_procs; //integer division dimrow/num_process 
  rem_r=dimrow%num_procs;
  
  
  if (my_id==root_process) { //root_process

    //initialization of the root_process' own local arrays
    arr_in=createArray2D (dimrow, dimcol);
    arr_out=createArray2D (dimrow, dimcol);
    arr_global=createArray2D (dimrow, dimcol);
    
    memset(arr_out, valreset, dimrow*dimcol*sizeof(TYPE));
    memset(arr_global, valreset, dimrow*dimcol*sizeof(TYPE));

    
    
    printf("... kernel side: %d (area: %f)\n", kernel_dim, norm);
    printf("... len_r_arr_comm: %d num_procs: %d\n", len_r_arr_comm, num_procs);

    if (modefill==0)
      srand(time(NULL));
    
    //filling the input array 
    for (indrow=0; indrow<dimrow;indrow++)
      { //loopA
	for (indcol=0; indcol<dimcol;indcol++)
	  { //loopB
	    if(modefill==0)
	      arr_in[indrow*dimcol+indcol]=(float)(rand() / (double)(RAND_MAX));
	    else if (modefill==1)
	      arr_in[indrow*dimcol+indcol]=1.f;
	    else if (modefill==2)
	      arr_in[indrow*dimcol+indcol]=(TYPE)cntelem;
	    cntelem++;
	  } //loopB
      }//loopA

    printf("arr_in:\n");
    printArray(arr_in, dimrow, dimcol, 0, 0);
    
    printf("\n\n");

    comp_filter(arr_in, arr_out, dimrow, dimcol, STEP);
    printf("arr_out:\n");
    printArray(arr_out, dimrow, dimcol, 0, 0);
    dumpArray (arr_out, dimrow, dimcol, file_arr_out); //dumping processed arr_out on a file to match later
    
  }//root_process

  //every process memory allocation (max size is len_r_arr_comm+2*STEP)
  arr_comm=createArray2D (len_r_arr_comm+2*STEP, dimcol);
  arr_res=createArray2D (len_r_arr_comm+2*STEP, dimcol);
  memset(arr_comm, valreset, (len_r_arr_comm+2*STEP)*dimcol*sizeof(TYPE));
  memset(arr_res, valreset, (len_r_arr_comm+2*STEP)*dimcol*sizeof(TYPE));
  
  //printf("[MPI process %d] I start waiting on the barrier.\n", my_id);
  MPI_Barrier(MPI_COMM_WORLD);

  int *sendcounts, *recvcounts, *displs, *displs_recv;
  sendcounts=malloc(num_procs*sizeof(int)); //how many bytes to send
  if (sendcounts== NULL){
    printf("... error in creating sendcounts array\n");
    exit(EXIT_FAILURE);
  }
  recvcounts=malloc(num_procs*sizeof(int)); //how many bytes to send
  if (recvcounts== NULL){
    printf("... error in creating recvcounts array\n");
    exit(EXIT_FAILURE);
  }
  displs=malloc(num_procs*sizeof(int)); //buffer(s) starting point
  if (displs== NULL){
    printf("... error in creating displs array\n");
    exit(EXIT_FAILURE);
  }
  displs_recv=malloc(num_procs*sizeof(int)); //buffer(s) starting point
  if (displs_recv== NULL){
    printf("... error in creating displs_recv array\n");
    exit(EXIT_FAILURE);
  }
  
  for(ind=0; ind<num_procs; ind++){
    if(ind==0){
      sendcounts[ind]=(len_r_arr_comm+STEP)*dimcol;
      displs[ind]=0;
      displs_recv[ind]=0;
      recvcounts[ind]=len_r_arr_comm*dimcol;
    }
    else if(ind<(num_procs-1) && ind>0){
      sendcounts[ind]=(len_r_arr_comm+2*STEP)*dimcol;
      displs[ind]=(ind*len_r_arr_comm-STEP)*dimcol;
      displs_recv[ind]=ind*len_r_arr_comm*dimcol;
      recvcounts[ind]=len_r_arr_comm*dimcol;
    }
    else if(ind==(num_procs-1)){
      int sizemod=( dimrow-(len_r_arr_comm*(num_procs-1))+STEP );
      sendcounts[ind]=sizemod*dimcol;
      displs[ind]=(ind*len_r_arr_comm-STEP)*dimcol;
      displs_recv[ind]=ind*len_r_arr_comm*dimcol;
      recvcounts[ind]=(len_r_arr_comm+rem_r)*dimcol;
    }
  }

  /* if (my_id==0){ */
  /*   for (ind=0; ind<num_procs; ind++){ */
  /*     printf("id %d - sendcounts: %d displs: %d recvcounts: %d displs_recv: %d\n", ind, sendcounts[ind], displs[ind], recvcounts[ind], displs_recv[ind]); */

  /*   } */
  /* } */


  MPI_Scatterv(arr_in, sendcounts, displs, MPI_FLOAT, arr_comm, (len_r_arr_comm+2*STEP)*dimcol, MPI_FLOAT, 0, MPI_COMM_WORLD);


  ///Every process performs computation
  if(my_id==0){
    comp_filter(arr_comm, arr_res, len_r_arr_comm+STEP, dimcol, STEP);
    //printf("\n\n");
    //printf("arr_res da process %d:\n", my_id);
    //printArray(arr_res, len_r_arr_comm+STEP, dimcol, 0, STEP);
  }
  
  
  if(my_id<(num_procs-1) && my_id>0){
    comp_filter(arr_comm, arr_res, len_r_arr_comm+2*STEP, dimcol, STEP);
    //printf("\n\n");
    //printf("arr_res da process %d:\n", my_id);
    //printArray(arr_res, len_r_arr_comm+2*STEP, dimcol, STEP, STEP);
  }

  //MPI_Barrier(MPI_COMM_WORLD);
  
  if(my_id==(num_procs-1)){
    comp_filter(arr_comm, arr_res, len_r_arr_comm+2*STEP, dimcol, STEP);
    //printf("\n\n");
    //printf("arr_res da process %d:\n", my_id);
    //printArray(arr_res, len_r_arr_comm+2*STEP, dimcol, STEP, (rem_r==0)?STEP:(STEP-rem_r));
    
  }

  ///Every local array starts with an halo of STEP*dimcol but the first one
  ///Every local array ends with an halo of STEP*dimcol but the last one
  ///Every process sends the same amount of data to root_process but the last one
  ///which might deal with the remainder of the integer division dimrow/num_process 

  
  int shift_start=(my_id==0)?0:(STEP*dimcol); //skipping the halo
  int dim_sendgather=(my_id<(num_procs-1)?len_r_arr_comm*dimcol:(len_r_arr_comm+rem_r)*dimcol); //the last process might have to send more data than the previous ones because of the remainder of the integer division dimrow/num_process (rem_r)
  
  MPI_Gatherv(arr_res+shift_start, dim_sendgather,  MPI_FLOAT, arr_global, recvcounts, displs_recv, MPI_FLOAT, 0, MPI_COMM_WORLD);
  
  if (my_id==root_process){
    printf("\n\n");
    printf("arr_global da process %d:\n", my_id);
    printArray(arr_global, dimrow, dimcol, 0, 0);
    dumpArray (arr_global, dimrow, dimcol, file_arr_global); //dumping processed arr_global on a file to match later with arr_out
  }

  
  /* MPI_Scatter(arr_in, (len_r_arr_comm+STEP)*dimcol, MPI_FLOAT, arr_comm, (len_r_arr_comm+STEP)*dimcol, MPI_FLOAT, 0, MPI_COMM_WORLD); */
  
  /* if (my_id==0){ */
  /*   comp_filter(arr_comm, arr_res, len_r_arr_comm+STEP, dimcol, STEP); */
  /*   printf("\n\n"); */
  /*   printf("arr_res da process %d:\n", my_id); */
  /*   printArray(arr_res, len_r_arr_comm, dimcol); */
  /* } */
  /* if (my_id==1){ */
  /*   comp_filter(arr_comm, arr_res, len_r_arr_comm+STEP, dimcol, STEP); */
  /*   printf("\n\n"); */
  /*   printf("arr_res da process %d:\n", my_id); */
  /*   printArray(arr_res, len_r_arr_comm, dimcol); */
  /* } */
  
  
  MPI_Barrier(MPI_COMM_WORLD);
  
  //every process free memory 
  
  free(arr_comm);
  free(arr_res);
  
  free(sendcounts);
  free(recvcounts);
  free(displs);
  free(displs_recv);
  
  if (my_id==root_process){ //root_process
    
    free(arr_in);
    free(arr_out);
    free(arr_global);
    
  } //root_process
  


  //MPI_Send(&messageItem, 1, MPI_INT, 1, 1, MPI_COMM_WORLD);
  //printf("process %i: Message %d sent\n", my_id, messageItem);
  
  
  ierr = MPI_Finalize();
  
  return(EXIT_SUCCESS);
}
